import sys
import logging
import rds_config
import pymysql
import os
import json

rds_host  = "grapefruit-test.ccrvynv2g4pg.us-east-1.rds.amazonaws.com"
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def get_conn():
    # returns db conn object
    
    try:
        logger.info("creating conn")
        conn = pymysql.connect( rds_host, 
                                user=name, 
                                passwd=password, 
                                db=db_name, 
                                connect_timeout=5)
        logger.info("conn created")
        return conn
    except:
        logger.error("ERROR CONNECTING TO DB")
        sys.exit()


def do_query(query, conn):
    logger.info("doing query")
    logger.info(query)
    with conn.cursor() as cur:
        cur.execute(query)
        if 'insert' in query.lower():
            conn.commit()
    logger.info('returning results')
    if cur._rows == None:
        return {'success':'yay'}
    return map(lambda x: [str(y) for y in x], cur._rows)


def make_response(d):
    res = { "isBase64Encoded": False,
            "statusCode": 200,
            "headers": {"Access-Control-Allow-Origin": "*"},
            "body": json.dumps(d)
            }
    return res


def ServiceAddHandler(event, context):
    #### MAIN ####
    logger.info('starting main')

    logger.info('calling helpers')
    conn = get_conn()

    params = event['queryStringParameters']

    if params is None:
        return make_response({'error':'i need params, homie'})


    service_name = params.get('service_name', None)
    ip = params.get('ip', None)
    port_num = params.get('port_num', None)
    protocol = params.get('protocol', None)

    if any([x == None for x in [service_name, ip, port_num, protocol]]):
        return make_response({'error':'service_name, ip, port_num, protocol are all required.'})
    else:
        query = ''' SELECT count(id) FROM service WHERE service_name = "%s" or port_num = %s; ''' % (service_name, port_num)
        res = do_query(query, conn)
        if res[0][0] != '0':
            return make_response(res[0])
            return make_response({'error':'service_name or port_num already exists'})
        else:
            query = ''' INSERT INTO service VALUES (NULL, "%s", "%s", %s, "%s"); ''' % (service_name, ip, port_num, protocol)
            
    return make_response(do_query(query, conn))
