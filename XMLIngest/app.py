import sys
import logging
import rds_config
import pymysql
import rds_config
import xmltodict
import boto3
import botocore.config
import os

rds_host = "grapefruit-test.ccrvynv2g4pg.us-east-1.rds.amazonaws.com"
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def get_conn():
    # returns db conn object

    try:
        logger.info("creating conn")
        conn = pymysql.connect(rds_host,
                               user=name,
                               passwd=password,
                               db=db_name,
                               connect_timeout=5)
        logger.info("conn created")
        return conn
    except BaseException:
        logger.error("ERROR CONNECTING TO DB")
        sys.exit()


def get_services(conn):
    # gets the latest services list from the database
    logger.info('getting services')

    res = {}

    with conn.cursor() as cur:
        cur.execute('select port_num, service_name, id from service;')
        for row in cur:
            res[int(row[0])] = {'name': row[1], 'id': int(row[2])}
    return res


def add_service(src_port, dst_port, src_ip, dst_ip, conn):
    # adds a record to the service table for a svc we're not yet tracking
    logger.info('adding service')

    query = ''' INSERT INTO service VALUES '''

    if src_port < dst_port:
        query += ''' (NULL, "UNK SVC %d", ''' % src_port
        query += ''' "%s", %d, ''' % (src_ip, src_port)

    else:
        query += ''' (NULL, "UNK SVC %d", ''' % dst_port
        query += ''' "%s", %d, ''' % (dst_ip, dst_port)

    query += ''' "TCP"); '''

    logger.info('attempting to insert new service to the table')
    with conn.cursor() as cur:
        cur.execute(query)
        conn.commit()

    query = ''' SELECT port_num, service_name, id FROM service '''
    query += ''' ORDER BY id DESC LIMIT 1; '''

    logger.info('getting new service info from db')
    with conn.cursor() as cur:
        cur.execute(query)
        res = cur._rows[0]

    return res


def XMLIngestHandler(event, context):
    #### MAIN ####

    logger.info('starting main')

    BUCKET_NAME = 'pcaps.shellcollecting.club'
    KEY = event['Records'][0]['s3']['object']['key']
    ROUND_NUM = int(KEY.split('/')[0].split('round')[1])

    logger.info('getting s3 client')
    s3_client = boto3.client(
        's3',
        'us-east-1',
        config=botocore.config.Config(s3={'addressing_style': 'path'}))
    response = s3_client.get_object(Bucket=BUCKET_NAME, Key=KEY)

    logger.info('getting xml file content')
    xml_file = response['Body'].read()

    logger.info('calling helpers')
    conn = get_conn()
    services = get_services(conn)

    logger.info('parsing xml file content')
    xml_dict = xmltodict.parse(xml_file, xml_attribs=True)

    errors = []
    successes = 0

    logger.info('starting main loop')
    for x in xml_dict['dfxml']['configuration'][1]['fileobject']:
        if x['filesize'] != u'0':
            file_path = x['filename']
            file_size = int(x['filesize'])
            start_time = x['tcpflow']['@startime']
            end_time = x['tcpflow']['@endtime']
            src_ip = x['tcpflow']['@src_ipn']
            dst_ip = x['tcpflow']['@dst_ipn']
            num_packets = int(x['tcpflow']['@packets'])
            src_port = int(x['tcpflow']['@srcport'])
            dst_port = int(x['tcpflow']['@dstport'])
            maybe_incomplete = 'false'

            if src_port in services:
                service_id = services[src_port]['id']
                maybe_incomplete = 'true'
            elif dst_port in services:
                service_id = services[dst_port]['id']
            else:
                new_svc = add_service(src_port, dst_port, src_ip, dst_ip, conn)
                services[int(new_svc[0])] = {'name': new_svc[1],
                                             'id': int(new_svc[2])}
                service_id = int(new_svc[2])

            query = '''INSERT INTO conversations VALUES '''
            query += ''' (NULL, "%s", ''' % file_path
            query += ''' %d, %d, ''' % (file_size, num_packets)
            query += ''' "%s", "%s", ''' % (start_time, end_time)
            query += ''' "%s", "%s", ''' % (src_ip, dst_ip)
            query += ''' %d, %d, ''' % (src_port, dst_port)
            query += ''' %d, %d, ''' % (ROUND_NUM, service_id)
            query += ''' false, %s); ''' % maybe_incomplete

            with conn.cursor() as cur:
                cur.execute(query)
                conn.commit()
                successes += 1
        # except Exception, e:
        #        errors.append(e)

    logger.info('finishing up')

    return {
        'errors': errors,
        'successes': successes
    }
