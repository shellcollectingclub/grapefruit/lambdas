import boto3
import json
from hexdump import hexdump

s3 = boto3.resource('s3')

def make_response(d):
    res = { "isBase64Encoded": False,
            "statusCode": 200,
            "headers": {"Access-Control-Allow-Origin": "*"},
            "body": d
            }
    return res

def lambda_handler(config, context):
    bucket = config["bucket"]
    key = config["key"]
    r = ""
    objs = list(s3.Bucket(bucket).objects.filter(Prefix=key).all())
    if len(objs) > 0 and objs[0].key == key:
        response = objs[0].get()
        contents = response['Body'].read()
        r = hexdump(contents,result="return")
    return make_response(r)