import sys
import logging
import rds_config
import pymysql
import socket
import rds_settings

rds_host = "grapefruit-test.ccrvynv2g4pg.us-east-1.rds.amazonaws.com"
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def TestLambdaHandler(event, context):
    try:
        conn = pymysql.connect(
            rds_host,
            user=name,
            passwd=password,
            db=db_name,
            connect_timeout=5)
    except BaseException:
        logger.error("ERROR CONNECTING TO DB")
        sys.exit()
    res = []
    with conn.cursor() as cur:
        cur.execute('select * from regex;')
        for row in cur:
            res.append(row)
    return {
        'results': res
    }
