import boto3
import json
import random
import resource
import StringIO
import time
import re
import os
import pymysql
import rds_config
import logging

from bloom_filter import BloomFilter
from shutil import copyfile

# setting database args
rds_host  = "grapefruit-test.ccrvynv2g4pg.us-east-1.rds.amazonaws.com"
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name

# get log object
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Create an S3 session
s3 = boto3.resource('s3')
s3_client = boto3.client('s3')

# Constants
# This is where the bloom filter is stored in the
#  mapper lambda context. Must be in /tmp/ because
#  working directory is readonly.
LOCAL_BLOOM = "/tmp/bfilter"

def get_conn():
    # returns db conn object
    try:
        logger.info("creating conn")
        conn = pymysql.connect( rds_host,
                                user=name,
                                passwd=password,
                                db=db_name,
                                connect_timeout=5)
        logger.info("conn created")
        return conn
    except:
        logger.error("ERROR CONNECTING TO DB")

def do_query(query, conn):
    # perform database query
    with conn.cursor() as cur:
        cur.execute(query)
        conn.commit()
    if cur._rows == None:
        return True
    else:
        return False

def keyname(prefix, fname, regx):
    """
    (str,str,str) -> str
    This function creates keys to query the bloom
     filter.
    The two types of queries will be:
    - Has this file/regx combination been run before?
      Denoted with a "R" prefix
    - Did this file/combination fail to match?
      Denoted with a "N" prefix
    The reason non-matches are recorded as opposed to
     matches is that theoretically good regx authors
     aim to match fewer things so they may pull out
     interesting data. Fewer matches puts less strain
     on the bloom filter.
    """
    return "{}||{}||{}".format(prefix,fname,regx)

def bloom_seen(fname, regx, bloom):
    """
    (str, str, BloomFilter) -> bool
    Ask if we have run this filename-regx combination
    """
    return keyname("R",fname,regx) in bloom

def bloom_match(fname, regx, bloom):
    """
    (str, str, BloomFilter) -> bool
    Ask if we have failed to match this filename-regx
     combination
    """
    return keyname("N",fname,regx) not in bloom

def make_bloom(members, error, location):
    """
    (int, float, str) -> BloomFilter
    """
    return BloomFilter(max_elements=members,
            error_rate=error,
            filename=location)

def insert_suspect(key,regx,conn):
    q = ''' INSERT INTO suspiciousness VALUES ('''
    q += ''' NULL, '''
    q += ''' (SELECT id FROM conversations WHERE file_path = "%s"), ''' % key
    q += ''' (SELECT id FROM regex WHERE pattern = "%s"), ''' % regx
    q += ''' (SELECT service FROM conversations WHERE file_path = "%s"), ''' % key
    q += ''' (SELECT round_num FROM conversations WHERE file_path = "%s")); ''' % key
    if not do_query(q, conn):
        logger.info('Failed to insert %s, %s into suspiciousness!' % (key, regx))

def lambda_handler(event, context):
    ######### MAIN #############
    start_time = time.time()
    # Extract information from event.
    src_bucket = event['bucket']
    src_keys = event['keys']
    mapper_id = event['mapperId']
    all_regx = event['regx']
    filter_members = event['filter_members']
    filter_error = event['filter_error']

    # These variables will aggregate data during the
    #  following for loop.
    output = {}
    line_count = 0
    err = ''

    # The filter is passed with the code. Due to the
    #  way zipping works, it appears in a tmp subdir.
    uploaded_filter = "{}/tmp/bfilter".format(
        os.environ['LAMBDA_TASK_ROOT'])
    # While we are not actually writing to the bloom
    #  filter, the library we are using requires the
    #  filter to be opened as writable. To circumvent
    #  this, we copy the readonly file to the tmp dir.
    copyfile(uploaded_filter,LOCAL_BLOOM)
    # Create a new bloom filter object
    bloom = make_bloom(filter_members, filter_error,
            LOCAL_BLOOM)
    # Pre-compile all the regx so we do not recompile
    #  them for every key.
    compiled_regx = map(re.compile, all_regx)
    # get db conn
    conn = get_conn()
    # Process all the keys.
    for key in src_keys:
        # First, see which key-regx combinations have
        #  been run before. We do this through the
        #  bloom filter.
        cache = [bloom_seen(key,r,bloom) for r in all_regx]
        if all(cache):
            # If every key was in the cache, we do not
            #  need to do an s3 GET. Simply ask the
            #  bloom filter what we found the last time
            #  we queried. Note: this does accept some
            #  risk in the form of saying a key-regx
            #  combination matched when it did not.
            output[key] = [bloom_match(key,r,bloom) for r in all_regx]
            continue
        # If not every key matched, we need to get the
        #  file contents so we can do regx matching
        response = s3_client.get_object(Bucket=src_bucket,
                Key=key)
        contents = response['Body'].read()
        del response
        for i, cregx in enumerate(compiled_regx):
            line_count += 1
            # I dont think we need the try block here,
            #  but I'm going to leave it anyway.
            try:
                # First, check to see if we have run
                #  this regx-key combination.
                if cache[i]:
                    # If so, return whether or not we
                    #  matched last time. Again this
                    #  can return a false-positive.
                    m = bloom_match(key,all_regx[i],bloom)
                else:
                    # FINALLY! The case where we have
                    #  a cache miss. Do re.search on
                    #  The contents of the file.
                    m = True if cregx.search(contents) else False
                    if m:
                        # Add the match to our database
                        try:
                            insert_suspect(key,all_regx[i],conn)
                        except Exception as e:
                            err += "No insert: {}\n".format(e)
                # Save the results into an array of
                #  the same order as the regx
                tmp = output.get(key, [])
                tmp.append(m)
                output[key] = tmp
            except Exception as e:
                err+=str(e)

    # Print out some fun stuff.
    time_in_secs = (time.time() - start_time)
    metadata = {
        "linecount": '%s' %
        line_count,
        "processingtime": '%s' %
        time_in_secs,
        "memoryUsage": '%s' %
        resource.getrusage(
            resource.RUSAGE_SELF).ru_maxrss}
    print metadata
    # Return the data driver will use.
    return json.dumps({"line_count":line_count,
            "time":time_in_secs,
            "errors":err,
            "output":output,
            "mem":metadata["memoryUsage"]})
