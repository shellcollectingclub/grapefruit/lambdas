### REGX MAPPER
Right now, the mapper works like this.
You pass in a json struct as in [driverconfig.json](driverconfig.json).
This struct provides [driver.py](driver.py) a list of [regular expressions](#regx) to run on every file that starts with a prefix in [_keys_](#keys).
Once running through every file starting with _keys_ and every provided _regx_, [driver.py](driver.py) caches the results of the run.
Within the [jobBucket](#jobBucket), [driver.py](driver.py) stores a file of the form `s3:/mapper-cache/bloom`.
The first thing [driver.py](driver.py) does is a look to see if each _regx-prefix_ combination has been completed before; if completed, a combination will not be re-run.


#### driverconfig.json

 - "bucket": OPTIONAL. This is the bucket where the mappers find data to process.
Defaults to pcaps.shellcollecting.club.
 - "keys": This is a list of prefixs to files within [bucket](#bucket).
For example, `[/pcaps/round/service/]`.
Every file in _bucket_ with this prefix will have regular expressions run on their contents.
 - "regx": _regx_ is a list of regular expressions.
Every file in [prefix](#prefix) will have every expression in _regx_ run on them.
This uses the python `re.search` method.

#### return

[driver.py](driver.py) returns a list of keynames that matched any of the provided regx.

#### Example:

CONFIG:
```
{
	  "bucket": "jocular-mapdata",
      "keys": ["testdata/"],
      "regx": [
            "EXAMPLE(.{2})",
            "XA."
        ]
} 
```

RETURN:
```
{
  "body": "[]",
  "headers": {
    "Access-Control-Allow-Origin": "*"
  },
  "method": "Distributed.",
  "isBase64Encoded": false,
  "statusCode": 200
}
```

SIDE EFFECTS:
`s3:/mapper-cache/bloom` contains:
Matching information about your job.