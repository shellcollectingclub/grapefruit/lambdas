import boto3
import json
import math
import time
import lambdautils
import glob
import os
import pymysql
import rds_config
import logging

from zipfile import ZipFile
from multiprocessing.dummy import Pool as ThreadPool
from functools import partial
import md5

from bloom_filter import BloomFilter
from mapper import bloom_match, bloom_seen, make_bloom, keyname

# setting database args
rds_host  = "grapefruit-test.ccrvynv2g4pg.us-east-1.rds.amazonaws.com"
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name

# get log object
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# create an S3 session
s3 = boto3.resource('s3')
s3_client = boto3.client('s3')
lambda_client = boto3.client('lambda')

#Location in S3 to store bloom filter
CACHE_DIR = "mapper_cache/bloom"
#Location on local (driver lambda instance)
# hard drive for the bloom filter
#Note that the bloom file will not be located
# in the lambda root folder as this folder is
# readonly and we must pull the file from S3.
# This means the file will exist in the zip as
# tmp/bfilter
LOCAL_BLOOM = "/tmp/bfilter"
LOCAL_VERSION = LOCAL_BLOOM+".version"
#Location to store the zipped mapper function and filter
MAPPER_ZIP = "/tmp/mapper.zip"
#These settings configure how large the bloom filter
# file will be. FILTER_MEMBERS is the maximum number of
# entries it can hold with the target FILTER_ERROR rate.
# 1,000,000 ~  4mb
# 6,000,000 ~ 25mb
FILTER_MEMBERS = 1000000
FILTER_ERROR = 0.000001

# ### UTILS ####
def zipLambda(fname, zipname, bloom_file):
    """
    (str,str, str) -> None
    This function creates a zipfile containing all code
     necessary to run the mapper (a function in fname).
    This includes the bloom filter, saved in bloom_file.
    The zipfile is saved as zipname.
    """
    print "Zipping mapper function..."
    all_files = [bloom_file,
        fname,
        "lambdautils.py",
        "bloom_filter.py",
        "python2x3.py",
        "rds_config.py"]

    with ZipFile(zipname, "w") as f:
        #write each file to the zip file
        for src in all_files:
            f.write(src)
        for root, dirs, files in os.walk("pymysql"):
            for file in files:
                f.write(os.path.join(root, file))

def get_conn():
    # returns db conn object
    try:
        logger.info("creating conn")
        conn = pymysql.connect( rds_host,
                                user=name,
                                passwd=password,
                                db=db_name,
                                connect_timeout=5)
        logger.info("conn created")
        return conn
    except:
        logger.error("ERROR CONNECTING TO DB")

def do_query(query, conn):
    # perform database query
    with conn.cursor() as cur:
        cur.execute(query)
        if 'insert' in query.lower():
            conn.commit()
    if cur._rows == None:
        return True

    return map(lambda x: [str(y) for y in x], cur._rows)

def make_response(d, method):
    """
    (dict[any]:any) -> dict[any]:any
    This turns return data into a string the API
     can process.
    """
    res = { "isBase64Encoded": False,
            "statusCode": 200,
            "headers": {
                "Access-Control-Allow-Origin": "*"},
            "body": json.dumps(transform(d)),
            "method": method
            }
    return res

def resolve_keys(bucket,all_keys):
    """
    (s3.Bucket, list[str]) -> list[s3.Object]
    """
    print "Resolving requested keys with S3..."
    def find_key(accum, key):
        return accum + list(s3.Bucket(
                bucket).objects.filter(Prefix=key))
    return reduce(find_key,all_keys,[])

def download_filter(bucket):
    """
    (s3.Bucket) -> None
    """
    try:
        print "Checking cache version..."
        data = ""
        # Make sure the local version file exists.
        if os.path.isfile(LOCAL_VERSION):
            with open(LOCAL_VERSION, "r") as f:
                data = f.read()
        # This get_object will error (304) if the
        #  ETag has not changed. We will use the
        #  local copy if this is the case.
        key = s3_client.get_object(Bucket=bucket,
                Key=CACHE_DIR,
                IfNoneMatch=data)
        # There was no error, so download the new
        #  version of the filter.
        print "Downloading bloom filter from S3..."
        s3_client.download_file(Bucket=bucket,
            Key=CACHE_DIR,
            Filename=LOCAL_BLOOM)
        # Save the version tag so we know when to
        #  update our local copy.
        with open(LOCAL_VERSION, "w") as f:
            f.write(key["ETag"])
    except Exception as e:
        print e
        # Ensure the file exists so when we zip we
        #  do not error.
        open(LOCAL_BLOOM,'a').close()

def cache_precheck(bloom,all_keys,regx):
    """
    (BloomFilter, list[s3.Object], list[str]) -> bool
    """
    print "Caching precheck..."
    return all(bloom_seen(key.key,r,bloom)
            for key in all_keys for r in regx)

def update_filter(total, regx, bloom, bucket):
    """
    (dict[str]:list[bool], list[str], BloomFilter,
     s3.Bucket) -> None
    """
    # Get the latest version (race condition still exists)
    download_filter(bucket)
    print "Updating local filter..."
    # For each file-regx combination, update the filter
    #  with the fact that we ran the combination.
    for fname in total:
        for i,ret in enumerate(total[fname]):
            bloom.add(keyname("R",fname,regx[i]))
            if not ret:
                # If the combination did not match, also
                #  add this data to the bloom filter
                bloom.add(keyname("N",fname,regx[i]))

def upload_filter(bucket):
    """
    (s3.Bucket) -> None
    """
    print "Writing filter to S3..."
    try:
        s3_client.upload_file(Bucket=bucket,
                Key=CACHE_DIR,
                Filename=LOCAL_BLOOM)
        key = s3_client.get_object(Bucket=bucket,
                Key=CACHE_DIR)
        with open(LOCAL_VERSION, "w") as f:
            f.write(key["ETag"])
    except Exception as e:
        print e
        pass

def insert_regx(regx, conn):
    """
    inserts regex into regex table if doesn't exist
    """
    q1 = '''SELECT count(id) FROM regex WHERE pattern = "%s" ''' % regx
    res = do_query(q1, conn)
    if res[0][0] != '0':
        logger.info('pattern %s already in db' % regx)
    else:
        q2 = ''' INSERT INTO regex VALUES (NULL, "%s", "auto added") ''' % regx
        res = do_query(q2, conn)
        if res != True:
            logger.info('Error inserting %s' % regx)
            logger.info('RES: %s' % json.dumps(res))

def transform(total):
    return filter(lambda k: any(total[k]), total)

def lambda_handler(config, env):
    ######### MAIN #############
    # 1. Get all keys to be processed
    mapper_name = "mapper.py"
    mapper_handle = "mapper.lambda_handler"
    job_id = "job{}".format(os.urandom(2).encode('hex'))
    bucket = config.get("bucket",
            "pcaps.shellcollecting.club")
    region = "us-east-1"
    lambda_memory = 256
    concurrent_lambdas = 80
    regx = config["regx"]
    all_keys = config["keys"]
    total = {}

    # Fetch all the keys that match the prefix and
    #  write them to all_keys
    all_keys = resolve_keys(bucket,all_keys)
    # If no keys were found, we may have the wrong
    #  bucket.
    if not len(all_keys):
        return make_response(total, "No known keys.")

    # Attempt to download the bloom filter. If it
    #  fails, we create an empty bloom filter by
    #  default.
    # Now also checks the local version against the
    #  s3 version and only downloads on difference.
    download_filter(bucket)

    # This precheck brings down the timing by about
    #  half when all keys are cached.
    bloom = make_bloom(FILTER_MEMBERS, FILTER_ERROR,
            LOCAL_BLOOM)
    if cache_precheck(bloom,all_keys,regx):
        print "No work to be done."
        for key in all_keys:
            for r in regx:
                tmp = total.get(key.key, [])
                tmp.append(bloom_match(key.key,r,bloom))
                total[key.key] = tmp
        return make_response(total, "All cached.")

    # Add each regx to our database.
    try:
        conn = get_conn()
        for r in regx:
            insert_regx(r, conn)
    except Exception as e:
        print "Error in main: {}".format(e)

    # Lambda functions must be zipped with all
    #  deps prior to spawning.
    zipLambda(mapper_name, MAPPER_ZIP, LOCAL_BLOOM)

    print "Batching data..."
    # Using the max allowed memory and the average
    #  size of s3 objects, determine how many keys
    #  each lambda function should process.
    bsize = lambdautils.compute_batch_size(all_keys,
            lambda_memory)
    bsize = min(bsize, 256)
    print "Batches of size {}".format(bsize)
    # Separate the keys into a list we can easily
    #  index during spawning.
    batches = lambdautils.batch_creator(all_keys, bsize)
    n_mappers = len(batches)
    del all_keys

    # 2. Spawn the lambda functions
    # This mapper_lambda_name is used when assigning
    #  the mappers names in the aws lambda interface.
    mapper_lambda_name = "BL-mapper-" + job_id

    # The lambdautils library helps upload the mapper code.
    print "Creating lambda function..."
    l_mapper = lambdautils.LambdaManager(
        lambda_client,
        s3_client,
        region,
        MAPPER_ZIP,
        job_id,
        mapper_lambda_name,
        mapper_handle,
        lambda_memory)
    l_mapper.update_code_or_create_on_noexist()

    ### Execute ###
    mapper_outputs = []
    # 3. Invoke Mappers
    def invoke_lambda(batches, m_id):
        """
        (list, int) -> None
        This function uses lambdautils.py to setup and
         call the mapper lambda.
        """
        batch = [k.key for k in batches[m_id - 1]]
        resp = lambda_client.invoke(
            FunctionName=mapper_lambda_name,
            InvocationType='RequestResponse',
            Payload=json.dumps({
                "bucket": bucket,
                "keys": batch,
                "mapperId": m_id,
                "regx": regx,
                "filter_members": FILTER_MEMBERS,
                "filter_error": FILTER_ERROR
            })
        )
        # This eval because lambda didn't return a nice
        #  json object ... thanks lambda.
        r = eval(resp['Payload'].read())
        out = json.loads(r)
        print "Mapper{} finished. Errors: {}\nUsed: {}".format(
                m_id,
                out["errors"],
                out["mem"])
        total.update(out["output"])
        # Instead of returning the results, append them
        #  to a list so we can thread this function.
        to_save = [out["line_count"],
                out["time"],
                out["errors"]]
        mapper_outputs.append(to_save)

    # For each batch, spawn a lambda. Ensure no more than
    #  concurrent_lambdas are invoked at one time. AWS lambda
    #  is run with only a single thread, but threading helps
    #  when blocked on networking. We also wait to recieve a
    #  response from each spawned lambda.
    print "# of Mappers ", n_mappers
    print "Starting Execution..."
    Ids = [i + 1 for i in range(n_mappers)]
    invoke_lambda_partial = partial(invoke_lambda, batches)
    pool = ThreadPool(n_mappers)
    mappers_executed = 0
    while mappers_executed < n_mappers:
        nm = min(concurrent_lambdas, n_mappers)
        results = pool.map(invoke_lambda_partial,
                Ids[mappers_executed: mappers_executed + nm])
        mappers_executed += nm
    pool.close()
    pool.join()

    print "All the mappers finished"
    # Delete Mapper function and cleanup variables
    l_mapper.delete_function()
    del pool
    del Ids
    del invoke_lambda_partial
    del batches
    del bsize
    del concurrent_lambdas

    # 4. Update the filter object in S3
    # Update the local bloom filter with the run's results
    update_filter(total, regx, bloom, bucket)

    # Finally, upload our cached filter to the s3 instance
    upload_filter(bucket)

    ######## COST ######
    # Calculate costs - Approx (since we are using exec
    # time reported by our func and not billed ms)
    total_lambda_secs = 0
    total_lines = 0

    for output in mapper_outputs:
        total_lines += int(output[0])
        total_lambda_secs += float(output[1])

    # Total Lambda costs
    lambda_cost = total_lambda_secs * 0.00001667 * lambda_memory / 1024.0

    # Print costs
    print "Lambda Cost", lambda_cost
    print "Total Lines:", total_lines

    #transform return

    # Format return code
    return make_response(total, "Distributed.")

if __name__ == "__main__":
    # emulate driver lambda call
    config = json.loads(open('driverconfig.json', 'r').read())
    r = lambda_handler(config, {})
    # print r
