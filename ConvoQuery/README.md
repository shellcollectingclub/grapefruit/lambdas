# ConvoQuery

## Amazon Lambda

This Lambda returns queries on the `conversations` table and is invoked via a GET request to /conversations. There is a built-in limit of 1000 unless `limit` is explicitly set. Any of the query groups can be combined. Below are the parameter groups:

### (convo_id_exact | (convo_id_range_start & convo_id_range_end)

`convo_id_exact` : the ID of the record to return

Example request: /conversations?convo_id_exact=1


`convo_id_range_start` : the start of the id range to query
`convo_id_range_end` : the end of the id range to query

Example request: /conversations?convo_id_range_start=1&convo_id_range_end=5



### (file_size_exact | (file_size_range_start & file_size_range_end))

`file_size_exact` : the size of the conversation pcap in bytes

Example request: /conversations?file_size_exact=512


`file_size_range_start` : the start of the conversation pcap file size range
`file_size_range_end` : the end of the conversation pcap file size range

Example request: /conversations?file_size_range_start=512&file_size_range_end=1024



### (num_packets_exact | (num_packets_range_start & num_packets_range_end))

`num_packets_exact` : the number of packets in the conversation pcap

Example request: /conversations?num_packets_exact=15


`num_packets_range_start` : the start of the range of the number of packets in the conversation pcap
`num_packets_range_end` : the end of the range of the number of packets in the conversation pcap

Example request: /conversations?num_packets_range_start=5&num_packets_range_end=10



### (time_start_exact | (time_start_range_start & time_start_range_end))

*NOTE* : This needs futher testing

`time_start_exact` : the timestamp of the start time of the pcap

`time_start_range_start` : the start of the range of the starting timestamp of the conversation pcap
`time_start_range_end` : the end of the range of the starting timestamp of the conversation pcap



### (time_end_exact | (time_end_range_start & time_end_range_end))

*NOTE* : This needs futher testing

`time_end_exact` : the timestamp of the end time of the pcap

`time_end_range_start` : the end of the range of the ending timestamp of the conversation pcap
`time_end_range_end` : the end of the range of the ending timestamp of the conversation pcap



### (src_ip)

`src_ip` : the source IP in the conversation pcap

Example request: /conversations?src_ip=127.0.0.1



### (dst_ip)

`dst_ip` : the destination IP in the conversation pcap

Example request: /conversations?dst_ip=127.0.0.1



### (round_num_exact | (round_num_range_start & round_num_range_end))

`round_num_exact` : the number of the round of the conversation

Example request: /conversations?round_num_exact=69


`round_num_range_start` : the start of the range of the round number of the conversation pcap
`round_num_range_end` : the end of the range of the round number of the conversation pcap

Example request: /conversations?round_num_range_start=69&round_num_range_end=71



### (services)

`services` : an *array* of foreign keys to the id of the service in the services table

Example request: /conversations?services[]=1,2,3



### (ignore_flag)

`ignore_flag` : a boolean indicating whether or not a conversation had been marked for being ignored

Example request: /conversations?ignore_flag=true



### (maybe_incomplete)

`maybe_incomplete` : a boolean indicating whether or not a conversation may be incomplete. This occurs when a pcap is completed, then resumed in the middle of a TCP session resulting in one of our service ports being marked as the source port. Our services really shouldn't be calling out unless it's been implanted (pretty unlikely).

Example request: /conversations?maybe_incomplete=true



### (offset)

`offset` : the OFFSET control word in MySQL

Example request: /conversations?offset=25



### (limit)

`limit` : the LIMIT control word in MySQL. This is set to 1000 by default unless explicitly set in this parameter.

Example request: /conversations?limit=500

