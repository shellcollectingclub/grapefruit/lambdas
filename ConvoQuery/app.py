import sys
import logging
import rds_config
import pymysql
import os
import json

rds_host  = "grapefruit-test.ccrvynv2g4pg.us-east-1.rds.amazonaws.com"
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def get_conn():
    # returns db conn object
    
    try:
        logger.info("creating conn")
        conn = pymysql.connect( rds_host, 
                                user=name, 
                                passwd=password, 
                                db=db_name, 
                                connect_timeout=5)
        logger.info("conn created")
        return conn
    except:
        logger.error("ERROR CONNECTING TO DB")
        sys.exit()


def do_query(query, conn):
    logger.info("doing query")
    logger.info(query)
    with conn.cursor() as cur:
        cur.execute(query)
    logger.info('returning results')
    return map(lambda x: [str(y) for y in x], cur._rows)


def make_response(d):
    res = { "isBase64Encoded": False,
            "statusCode": 200,
            "headers": {"Access-Control-Allow-Origin": "*"},
            "body": json.dumps(d)
            }
    return res


def add_param(field, val, and_flag, val_type = int):
    query = ''
    if and_flag:
        query += ''' AND '''
    if val_type != int:
        query += ''' %s = "%s" ''' % (field, val)
    else:
        query += ''' %s = %s ''' % (field, val)
    return query


def exact_range(exact, range_start, range_end, field, and_flag):
    
    query = ''
    if and_flag:
        query += ''' AND '''
    if exact:
        query += ''' %s = %s ''' % (field, exact)
    else:
        query += ''' %s >= %s AND %s <= %s ''' % (field, 
                                                range_start, 
                                                field, 
                                                range_end)
    return query


def ConvoQueryHandler(event, context):
    #### MAIN ####
    logger.info('starting main')

    logger.info('calling helpers')
    conn = get_conn()

    query = ''' SELECT * FROM conversations '''

    params = event['queryStringParameters']

    limit = params.get('limit', 1000)

    if params is None:
        query += ''' LIMIT %d ''' % limit
        return make_response(do_query(query, conn, False))

    count_only = params.get('count_only', None)

    if count_only:
        query = ''' SELECT count(id) FROM conversations '''

    convo_id_exact = params.get('convo_id_exact', None)
    convo_id_range_start = params.get('convo_id_range_start', None)
    convo_id_range_end = params.get('convo_id_range_end', None)
    
    file_size_exact = params.get('file_size_exact', None)
    file_size_range_start = params.get('file_size_range_start', None)
    file_size_range_end = params.get('file_size_range_end', None)

    num_packets_exact = params.get('num_packets_exact', None)
    num_packets_range_start = params.get('num_packets_range_start', None)
    num_packets_range_end = params.get('num_packets_range_end', None)
    
    time_start_exact = params.get('time_start_exact', None)
    time_start_range_start = params.get('time_start_range_start', None)
    time_start_range_end = params.get('time_start_range_end')

    time_end_exact = params.get('time_end_exact', None)
    time_end_range_start = params.get('time_end_range_start', None)
    time_end_range_end = params.get('time_end_range_end', None)

    src_ip = params.get('src_ip', None)

    dst_ip = params.get('dst_ip', None)

    src_port = params.get('src_port', None)

    dst_port = params.get('dst_port', None)

    round_num_exact = params.get('round_num_exact', None)
    round_num_range_start = params.get('round_num_range_start', None)
    round_num_range_end = params.get('round_num_range_end', None)

    services = params.get('services', None)
    if services:
        service = map(int, services)
        
    ignore_flag = params.get('ignore_flag', None)

    maybe_incomplete = params.get('maybe_incomplete', None)

    offset = params.get('offset', None)    

    if not any([convo_id_exact, convo_id_range_start, convo_id_range_end, 
                file_size_exact, file_size_range_start, file_size_range_end, 
                num_packets_exact, num_packets_range_start, 
                num_packets_range_end, time_start_exact, 
                time_start_range_start, time_start_range_end, time_end_exact, 
                time_end_range_start, time_end_range_end, src_ip, dst_ip, 
                src_port, dst_port, round_num_exact, round_num_range_start, 
                round_num_range_end, services, ignore_flag, maybe_incomplete
                ]):
        if not count_only:
            if offset:
                query += ''' LIMIT %d OFFSET %d ''' % (limit, offset)
            else:
                query += ''' LIMIT %d ''' % limit
            query += '''; '''
        return make_response(do_query(query, conn))
    else:
        query += ''' WHERE '''

    and_flag = False
    
    if convo_id_exact or (convo_id_range_start and convo_id_range_end):
        query += exact_range(convo_id_exact, 
                        convo_id_range_start, 
                        convo_id_range_end, 
                        'id', 
                        and_flag)
        and_flag = True
    
    if file_size_exact or (file_size_range_start and file_size_range_end):
        query += exact_range(file_size_exact, 
                        file_size_range_start, 
                        file_size_range_end, 
                        'file_size', 
                        and_flag)
        and_flag = True

    if num_packets_exact or (num_packets_range_start and num_packets_range_end):
        query += exact_range(num_packets_exact, 
                        num_packets_range_start, 
                        num_packets_range_end, 
                        'num_packets', 
                        and_flag)
        and_flag = True

    if time_start_exact or (time_start_range_start and time_end_range_end):
        query += exact_range(time_start_exact, 
                        time_start_range_start, 
                        time_start_range_end, 
                        'time_start', 
                        and_flag)
        and_flag = True

    if time_end_exact or (time_end_range_start and time_end_range_end):
        query += exact_range(time_end_exact, 
                        time_end_range_start, 
                        time_end_range_end, 
                        'time_end', 
                        and_flag)
        and_flag = True

    if src_ip:
        query += add_param('src_ip', src_ip, and_flag, str)
        and_flag = True

    if dst_ip:
        query += add_param('dst_ip', dst_ip, and_flag, str)
        and_flag = True
        
    if src_port:
        query += add_param('src_ip', src_port, and_flag)
        and_flag = True

    if dst_port:
        query += add_param('dst_port', dst_port, and_flag)
        and_flag = True

    if round_num_exact or (round_num_range_start and round_num_range_end):
        query += exact_range(round_num_exact, 
                        round_num_range_start, 
                        round_num_range_end, 
                        'round_num', 
                        and_flag)
        and_flag = True

    if services:
        for s in services:
            query += add_param('service', s, False)
            query += ''' OR '''
        query = query[:-3]
        and_flag = True

    if ignore_flag:
        query += add_param('ignore_flag', ignore_flag, and_flag)
        and_flag = True

    if maybe_incomplete:
        query += add_param('maybe_incomp', maybe_incomplete, and_flag)
        and_flag = True

    if not count_only:
        query += ''' LIMIT %d ''' % limit
    
        if offset:
            query += ''' OFFSET %d ''' % offset
    
    query += '''; '''

    return make_response(do_query(query, conn))
