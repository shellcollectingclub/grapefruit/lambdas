import sys
import logging
import rds_config
import pymysql
import os
import json

rds_host  = "grapefruit-test.ccrvynv2g4pg.us-east-1.rds.amazonaws.com"
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def get_conn():
    # returns db conn object
    
    try:
        logger.info("creating conn")
        conn = pymysql.connect( rds_host, 
                                user=name, 
                                passwd=password, 
                                db=db_name, 
                                connect_timeout=5)
        logger.info("conn created")
        return conn
    except:
        logger.error("ERROR CONNECTING TO DB")
        sys.exit()


def do_query(query, conn):
    logger.info("doing query")
    logger.info(query)
    with conn.cursor() as cur:
        cur.execute(query)
        if 'insert' in query.lower():
            conn.commit()
    logger.info('returning results')
    if cur._rows == None:
        return {'success':'yay'}
    return map(lambda x: [str(y) for y in x], cur._rows)


def make_response(d):
    res = { "isBase64Encoded": False,
            "statusCode": 200,
            "headers": {"Access-Control-Allow-Origin": "*"},
            "body": json.dumps(d)
            }
    return res


def ServiceGetHandler(event, context):
    #### MAIN ####
    logger.info('starting main')

    logger.info('calling helpers')
    conn = get_conn()

    query = ''' SELECT * from service; '''
    return make_response(do_query(query, conn))
