import boto3
import json
import time
import os
import stat
import shutil
import logging
import subprocess
from base64 import b64decode
import md5

from bloom_filter import BloomFilter

logger = logging.getLogger()
logger.setLevel(logging.INFO)

LOCAL_BINARY = "/tmp/binary"
LOCAL_TESTCASE = "/tmp/testcase"
TRACE_FILE = "/tmp/log"
LOCAL_FILTER = "/tmp/bloom"

FILTER_MEMBERS = 1000000
FILTER_ERROR = 0.000001

bucket = "pcaps.shellcollecting.club"
s3 = boto3.resource('s3')
s3_client = boto3.client('s3')

def make_response(d, method, status=200):
    """
    (dict[any]:any) -> dict[any]:any
    This turns return data into a string the API
     can process.
    """
    res = { "isBase64Encoded": False,
            "statusCode": status,
            "headers": {
                "Access-Control-Allow-Origin": "*"},
            "body": json.dumps(d),
            "method": method
            }
    return res

def load_data(event,*keys):
    logging.debug("load_data:: Looking for keys: %s", keys)
    try:
        data = json.loads(event["body"])
        if ((type(data) != dict) or 
            any(((key not in data) or 
                (type(data[key]) != unicode)) for key in keys)):
            raise TypeError("Incorrect body type: {}".format(data))
        logger.debug("load_data:: POSTED: %s", data)
        return [data[key] for key in keys]

    except Exception as e:
        logging.warning("load_data:: Error loading request body: %s", e)
        return [None for _ in keys]

def resolve_keys(prefix):
    return list(s3.Bucket(
        bucket).objects.filter(Prefix=prefix))

def download_file(s3key,local_location):
    logging.info("download_file:: Downloading: %s", s3key)
    try:
        local_version = "{}.version".format(local_location)
        logging.debug("download_file:: Checking local version: %s", 
                local_version)
        data = ""
        # Make sure the local version file exists.
        if os.path.isfile(local_version):
            with open(local_version, "r") as f:
                data = f.read()
        # This get_object will error (304) if the
        #  ETag has not changed. We will use the
        #  local copy if this is the case.
        key = s3_client.get_object(Bucket=bucket,
                Key=s3key,
                IfNoneMatch=data)
        # There was no error, so download the new
        #  version of the filter.
        logging.debug("download_file:: Pulling file from S3...")
        s3_client.download_file(Bucket=bucket,
            Key=s3key,
            Filename=local_location)
        # Save the version tag so we know when to
        #  update our local copy.
        with open(local_version, "w") as f:
            f.write(key["ETag"])
        logging.info("download_file:: Download complete.")
    except Exception as e:
        logging.warning("download_file:: Unable to download: %s", e)
        open(local_location,'a').close()

def download_service(key):
    download_file(key,LOCAL_BINARY)
    st = os.stat(LOCAL_BINARY)
    os.chmod(LOCAL_BINARY, st.st_mode | stat.S_IEXEC)

def download_testcase(key):
    download_file(key.key,LOCAL_TESTCASE)

def download_filter(fresh,filter_key):
    if fresh:
        if os.path.isfile(LOCAL_FILTER):
            os.remove(LOCAL_FILTER)
    else:
        #download correct filter (generate key)
        download_file(filter_key,LOCAL_FILTER)
    return BloomFilter(max_elements=FILTER_MEMBERS,
                error_rate=FILTER_ERROR,
                filename=LOCAL_FILTER)

def upload_filter(filter_key):
    logger.info("upload_filter:: Uploading bloom filter to %s", filter_key)
    try:
        local_version = "{}.version".format(LOCAL_FILTER)
        s3_client.upload_file(Bucket=bucket,
                Key=filter_key,
                Filename=LOCAL_FILTER)
        key = s3_client.get_object(Bucket=bucket,
                Key=filter_key)
        with open(local_version, "w") as f:
            f.write(key["ETag"])
        logger.info("upload_filter:: Upload complete.")
    except Exception as e:
        logger.warning("upload_filter:: Error uploading bloom filter: %s", e)

def analyze_trace(save, bloom):
    logger.info("analyze_trace:: Reviewing tracefile...")
    count = 0
    with open(TRACE_FILE, "r") as f:
        line = f.readline()
        while line:
            count+=1
            if save:
                #update bloom
                bloom.add(line)
            else:
                #query bloom
                if line not in bloom:
                    logger.info("analyze_trace:: Found unique entry after %s lines.", count)
                    return True
            line = f.readline()
    logger.info("analyze_trace:: Read %s lines. None unique.",count)
    return False

def run_service(cmd):
    logger.info("run_service:: Running trace.")
    logging.debug("run_service:: CMD: {}".format(cmd))
    try:
        f = open(LOCAL_TESTCASE, "rb")
        subprocess.call(cmd.split(),stdin=f)
        f.close()
    except Exception as e:
        logging.warning("run_service:: Error running service: %s", e)
        return False
    logging.info("run_service:: Trace complete.")
    return True

def lambda_handler(event,context):
    logger.debug("lambda_handler:: Event %s",event)
    method = event.get("httpMethod","")
    resource = event.get("resource","")
    logger.info("lambda_handler:: Method: %s", method)
    logger.info("lambda_handler:: Resource: %s", resource)
    initial = resource == "/initial"
    if (method == "POST" and (initial or
            resource == "/query")):
        (prefix,service) = load_data(event, "prefix", "service")
        if prefix:
            logger.info("lambda_handler:: Prefix translated. %s bytes.", len(prefix))
            filter_key="filters/{}".format(
                        md5.new(service).digest().encode("hex"))
            download_service(service)
            bloom = download_filter(initial,filter_key)
            cmd = """./afl/afl-showmap -q -Q -o {} -- {}""".format(
                    TRACE_FILE,
                    LOCAL_BINARY)
            keys = resolve_keys(prefix)
            unique = []
            for key in keys:
                download_testcase(key)
                if run_service(cmd):
                    if analyze_trace(initial,bloom):
                        unique.append(key.key)
            if initial:
                upload_filter(filter_key)
                return_msg = "{} files processed for {}.".format(
                        len(keys),
                        service)
                return make_response(return_msg,method)
            else:
                return make_response(unique,method)


    return make_response("Unknown page or malformed body.", method, 404)
